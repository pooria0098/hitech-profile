from django import forms
from .models import News, MyOrder


class NewsForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['email'].widget.attrs.update(
            {'class': 'email',
             'placeholder': 'ایمیل',
             'required': 'required'}
        )

    class Meta:
        model = News
        fields = '__all__'
        widgets = {
            'email': forms.EmailInput(),
        }


class OrderForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['f_name'].widget.attrs.update(
            {'class': 'text-box-dark',
             'placeholder': 'نام',
             'onfocus': 'this.value = str();',
             'onblur': 'if (this.value == str()) {this.value = str(First Name);}',
             }
        )
        self.fields['l_name'].widget.attrs.update(
            {'class': 'text-box-dark',
             'placeholder': 'نام خانودگی',
             'onfocus': 'this.value = str();',
             'onblur': 'if (this.value == str()) {this.value = str(Last Name);}',
             }
        )
        self.fields['email'].widget.attrs.update(
            {'class': 'text-box-dark',
             'placeholder': 'ایمیل',
             'onfocus': 'this.value = str();',
             'onblur': 'if (this.value == str()) {this.value = str(Email);}',
             }
        )
        self.fields['phone_number'].widget.attrs.update(
            {'class': 'text-box-dark',
             'placeholder': 'تلفن',
             'onfocus': 'this.value = str();',
             'onblur': 'if (this.value == str()) {this.value = str(Phone);}',
             }
        )
        self.fields['cart_number'].widget.attrs.update(
            {'class': 'text-box-dark',
             'placeholder': 'شماره کارت',
             'onfocus': 'this.value = str();',
             'onblur': 'if (this.value == str()) {this.value = str(Card Number);}',
             }
        )
        self.fields['cart_name'].widget.attrs.update(
            {'class': 'text-box-dark',
             'placeholder': 'نام روی کارت',
             'onfocus': 'this.value = '';',
             'onblur': "if (this.value == '') {this.value = 'Name on card';}",
             }
        )
        self.fields['expired_date'].widget.attrs.update(
            {'class': 'text-box-light hasDatepicker',
             'id': 'datepicker',
             'placeholder': 'تاریخ انقضا',
             'onfocus': 'this.value = '';',
             'onblur': "if (this.value == '') {this.value = 'Expiration Date';}",
             }
        )
        self.fields['password'].widget.attrs.update(
            {'class': 'text-box-dark',
             'placeholder': 'کد امنیتی',
             'onfocus': 'this.value = str();',
             'onblur': 'if (this.value == str()) {this.value = str(Security Code);}',
             }
        )

    class Meta:
        model = MyOrder
        fields = '__all__'
        widgets = {
            # 'password': forms.PasswordInput(),
            # 'email': forms.EmailInput(),
            # 'phone_number': forms.NumberInput(),
            # 'cart_number': forms.NumberInput(),
            # 'expired_date': forms.DateTimeInput()
            'expired_date':forms.SelectDateWidget()
        }
