from django.contrib import admin
from .models import MySkill, MyService, MyTeam, MyProject, News, MySocial, MyPrice, MyOrder

admin.site.register(MySkill)
admin.site.register(MyService)
admin.site.register(MyTeam)
admin.site.register(MyProject)
admin.site.register(News)
admin.site.register(MySocial)
admin.site.register(MyPrice)
admin.site.register(MyOrder)
