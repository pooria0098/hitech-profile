from PIL import Image
from django.contrib.auth.models import AbstractUser
from django.db import models
from ckeditor.fields import RichTextField


class MyTeam(AbstractUser):
    avatar = models.ImageField(upload_to='team/', default='user-01.jpeg', null=True, blank=True)
    profession = models.CharField(max_length=20)

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        img = Image.open(self.avatar.path)
        if img.height > 400 or img.width > 400:
            output_size = (300, 300)
            img.thumbnail(output_size)
            img.save(self.avatar.path)

    def __str__(self):
        return self.username


class MySkill(models.Model):
    name = models.CharField(max_length=20)
    percentage = models.IntegerField(default=50)

    def __str__(self):
        return self.name


class MyService(models.Model):
    title = models.CharField(max_length=200)
    description = RichTextField()
    img = models.ImageField(upload_to='service/', default='')

    def __str__(self):
        return self.title


class MyProject(models.Model):
    name = models.CharField(max_length=20)
    img = models.ImageField(upload_to='project/', default='')
    url = models.URLField(null=True, blank=True)

    def __str__(self):
        return self.name


class News(models.Model):
    email = models.CharField(max_length=200)

    def __str__(self):
        return self.email

    class Meta:
        verbose_name_plural = 'News'


class MySocial(models.Model):
    img = models.ImageField(upload_to='social/', default='')


class MyPrice(models.Model):
    price = models.IntegerField()
    title = models.CharField(max_length=30)
    description_1 = models.CharField(max_length=100)
    description_2 = models.CharField(max_length=100)
    description_3 = models.CharField(max_length=100)
    description_4 = models.CharField(max_length=100)

    def __str__(self):
        return self.title


class MyOrder(models.Model):
    f_name = models.CharField(max_length=20)
    l_name = models.CharField(max_length=20)
    email = models.CharField(max_length=30)
    phone_number = models.CharField(max_length=13)
    cart_number = models.CharField(max_length=20)
    cart_name = models.CharField(max_length=50)
    expired_date = models.CharField(max_length=10)
    password = models.CharField(max_length=6)

    def __str__(self):
        return self.f_name + ' ' + self.l_name


def get_all_services():
    return MyService.objects.all()


def get_all_members():
    return MyTeam.objects.all()


def get_all_prices():
    return MyPrice.objects.all()


def get_all_skills():
    return MySkill.objects.all()


def get_all_projects():
    return MyProject.objects.all()


def get_all_socials():
    return MySocial.objects.all()
